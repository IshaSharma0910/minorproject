function legitimateSite()
{
	document.getElementById("banner").innerHTML="<br>The site you are visiting is legitimate";
	document.getElementById("searchgif").src="check1.gif";
	document.getElementById("banner").style.backgroundColor= "#B2FB4D";
}
function phishingSite(site)
{
	document.getElementById("banner").innerHTML="<br>The site you are visiting is a phishing site</a>";
	document.getElementById("searchgif").src="wrong.png";
	document.getElementById("searchgif").style.height="160px";
	document.getElementById("searchgif").style.marginTop="25px";
	document.getElementById("banner").style.backgroundColor= "#FA8072";
}
function error()
{
	document.getElementById("banner").innerHTML="<br>Oops! We couldn't reach you.";
	document.getElementById("banner").style.backgroundColor= "#FA8072";
}
function execute() {
	var tablink = "global";
	chrome.tabs.getSelected(null, function (tab) {
		tablink = tab.url;
		var xhttp = new XMLHttpRequest();
		xhttp.open("POST", "http://localhost/clientServer.php", true);
		xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				var result = JSON.parse(this.responseText);
				if(result[result.length-1] == "yes")
					legitimateSite();
				else if(result[result.length-1]=="no")
					phishingSite();
				else
					error();						
			}
		};
		xhttp.withCredentials = false;
		xhttp.send("url=" + tablink);
	});
}

document.addEventListener('DOMContentLoaded', function () {
	execute();
});






