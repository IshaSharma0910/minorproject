from sklearn.feature_extraction.text import TfidfVectorizer
import xlrd

def getKeywords(url):
    loc = (r"H:\code\minorproject\ServerPhishingDetection\Book1.xlsx")
    book1 = xlrd.open_workbook(loc)
    read_sheet = book1.sheet_by_index(0)  
    corpus = []
    for i in range(len(read_sheet.col_values(0))):
        corpus.append(read_sheet.cell_value(i,3))
        if ( read_sheet.cell_value(i,0) == url ):
            curr_val = i


    vectorizer = TfidfVectorizer()
    response = vectorizer.fit_transform(corpus)
    names = vectorizer.get_feature_names()

    sorted_indices = (-response.toarray()).argsort()
    sorted_names = []

    keywords = ""
    for j in range (min(3,len(sorted_indices[i]))):
        print(corpus[curr_val], names[sorted_indices[curr_val,j]], response[curr_val,sorted_indices[curr_val,j]])
        keywords += names[sorted_indices[curr_val,j]] + " "
    return keywords
