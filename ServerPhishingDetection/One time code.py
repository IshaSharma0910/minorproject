import openpyxl
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tokenize.treebank import TreebankWordDetokenizer
import tldextract

from googlesearch import search
from html.parser import HTMLParser
from urllib.request import urlopen
import urllib
import requests
from bs4 import BeautifulSoup
from time import sleep
import sys

'''
non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)


def error_callback(*_, **__):
    pass

def is_string(data):
    return isinstance(data, str)

def is_bytes(data):
    return isinstance(data, bytes)

def to_ascii(data):
    if is_string(data):
        data = data.encode('ascii', errors='ignore')
    elif is_bytes(data):
        data = data.decode('ascii', errors='ignore')
    else:
        data = str(data).encode('ascii', errors='ignore')
    return data


class Parser(HTMLParser):
    def __init__(self, url):
        self.title = None
        self.rec = False
        HTMLParser.__init__(self)
        try:
            self.feed(to_ascii(urlopen(url).read()))
        except urllib.error.HTTPError:
            return
        except urllib.error.URLError:
            return
        except ValueError:
            return

        self.rec = False
        self.error = error_callback

    def handle_starttag(self, tag, attrs):
        if tag == 'title':
            self.rec = True

    def handle_data(self, data):
        if self.rec:
            self.title = data

    def handle_endtag(self, tag):
        if tag == 'title':
            self.rec = False
            
def getTitle(url):
    if Parser(url).title!=None :
        return Parser(url).title
    else :
        return " "
def getMetaDescription(url):       
    response = requests.get(url)
    soup = BeautifulSoup(response.text)
    metas = soup.find_all('meta')
    META = [meta.attrs['content'] for meta in metas if 'name' in meta.attrs and meta.attrs['name'] == 'description']
    if META != None:
        return str(META)[2:-2]
    else:
        return " "

def getText(url):

    html = requests.get(url)
        
    tree = BeautifulSoup(html.text, 'lxml')

    body = tree.body
    if body is None:
        return " "

    for tag in body.select('script'):
        tag.decompose()
    for tag in body.select('style'):
        tag.decompose()

    text = body.get_text(separator='\n')
    return text
'''
loc = (r"H:\code\minorproject\ServerPhishingDetection\Book1.xlsx")
book1 = openpyxl.load_workbook(loc)
sheet = book1["Sheet1"]


for i in range (31,sheet.max_row +1):
    '''TITLE = getMetaDescription(sheet.cell(i,1).value)
    print(TITLE)
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(TITLE)
    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    detokenizer = TreebankWordDetokenizer()
    TITLE = detokenizer.detokenize(filtered_sentence)'''
    sheet.cell(i,5,sheet.cell(i,5).value[2:-2])
    book1.save(loc)
'''
for i in range (1,sheet.max_row +1):
    TITLE = getTitle(sheet.cell(i,1).value)
    print(TITLE)
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(TITLE)
    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    detokenizer = TreebankWordDetokenizer()
    TITLE = detokenizer.detokenize(filtered_sentence)
    sheet.cell(i,4,TITLE)
    book1.save(loc)
for i in range (1,sheet.max_row +1):
    TITLE = getText(sheet.cell(i,1).value)
    TITLE=TITLE.translate(non_bmp_map)
    print(TITLE)
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(TITLE)
    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    detokenizer = TreebankWordDetokenizer()
    TITLE = detokenizer.detokenize(filtered_sentence)
    sheet.cell(i,6,TITLE)
    book1.save(loc)
  '''

