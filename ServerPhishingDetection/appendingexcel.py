import openpyxl
import nltk
from nltk.corpus import stopwords
#nltk.download('stopwords')
#nltk.download('punkt')
from nltk.tokenize import word_tokenize
from nltk.tokenize.treebank import TreebankWordDetokenizer
import tldextract   
import xlrd

def storeResult(text,url):
    loc = (r"H:\code\minorproject\ServerPhishingDetection\Book1.xlsx")
    book1 = openpyxl.load_workbook(loc)
    sheet = book1["Sheet1"]
    for i in range(1,sheet.max_row+1):
        if (sheet.cell(i,1).value == url ):
            row=i
    sheet.cell(row, 3, text)
    book1.save(loc)


def getDomainName(url):
    return tldextract.extract(url).registered_domain

def removeStopwords(TEXT):
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(TEXT)
    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    detokenizer = TreebankWordDetokenizer()
    return detokenizer.detokenize(filtered_sentence)
    
    
def addInfo(URL,TITLE,META,TEXT):
    TITLE = removeStopwords(str(TITLE))
    META = removeStopwords(str(META))
    TEXT = removeStopwords(str(TEXT))
    loc = (r"H:\code\minorproject\ServerPhishingDetection\Book1.xlsx")
    book1 = openpyxl.load_workbook(loc)
    sheet = book1["Sheet1"]  

    flag = False
    for i in range (1,sheet.max_row +1):
        sheet.cell(i,1).value
        if (URL == sheet.cell(i,1).value):
            flag = True
    if ( flag == False):
        sheet.cell(sheet.max_row +1, 1, URL)
        sheet.cell(sheet.max_row, 4, TITLE)
        sheet.cell(sheet.max_row, 5, META[2:-2])
        sheet.cell(sheet.max_row, 6, TEXT)
    book1.save(loc)
    
    
