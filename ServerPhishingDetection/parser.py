from googlesearch import search
from html.parser import HTMLParser
from urllib.request import urlopen
import urllib
import searching
import tfidf
import modified_tfidf
import appendingexcel
import tldextract
import requests
from bs4 import BeautifulSoup
import sys
import warnings
warnings.filterwarnings("ignore")
URL=sys.argv[1]
non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)

def error_callback(*_, **__):
    pass

def is_string(data):
    return isinstance(data, str)

def is_bytes(data):
    return isinstance(data, bytes)

def to_ascii(data):
    if is_string(data):
        data = data.encode('ascii', errors='ignore')
    elif is_bytes(data):
        data = data.decode('ascii', errors='ignore')
    else:
        data = str(data).encode('ascii', errors='ignore')
    return data


class Parser(HTMLParser):
    def __init__(self, url):
        self.title = None
        self.body=None
        self.rec = False
        HTMLParser.__init__(self)
        try:
            self.feed(to_ascii(urlopen(url).read()))
        except urllib.error.HTTPError:
            return
        except urllib.error.URLError:
            return
        except ValueError:
            return

        self.rec = False
        self.error = error_callback

    def handle_starttag(self, tag, attrs):
        if tag == 'title':
            self.rec = True
            
    def handle_data(self, data):
        if self.rec:
            self.title = data

    def handle_endtag(self, tag):
        if tag == 'title':
            self.rec = False
        
def getDomainName(url):
    return tldextract.extract(url).registered_domain

def getTitle(url):
    if Parser(url).title!=None :
        return Parser(url).title
    else :
        return " "

def getMetaDescription(url):       
    response = requests.get(url)
    soup = BeautifulSoup(response.text)
    metas = soup.find_all('meta')
    META = [ meta.attrs['content'] for meta in metas if 'name' in meta.attrs and meta.attrs['name'] == 'description' ]
    if META != None:
        return META
    else:
        return " "
    
def getText(url):
    html = requests.get(url)
    tree = BeautifulSoup(html.text, 'lxml')

    body = tree.body
    if body is None:
        return " "

    for tag in body.select('script'):
        tag.decompose()
    for tag in body.select('style'):
        tag.decompose()

    text = body.get_text(separator='\n')
    return text

DOMAIN_NAME = getDomainName(URL)
TEXT = getText(URL)
TEXT=TEXT.translate(non_bmp_map)
TITLE = getTitle(URL)
META = getMetaDescription(URL)
appendingexcel.addInfo(URL,TITLE,META,TEXT)
keywords=modified_tfidf.getKeywords(URL,TITLE,META)
RESULT=searching.searches(URL,keywords)
print(RESULT)
