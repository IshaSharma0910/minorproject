from googlesearch import search
import tldextract
import appendingexcel

def getDomainName(url):
    return tldextract.extract(url).registered_domain

def searches(url,keywords):
    domain_name = getDomainName(url)
    query = domain_name + " " + keywords
    results = search(query, num = 6, stop = 6, pause = 2)
    flag = False

    for result in results:
        if ( domain_name == getDomainName(result)) :
            flag = True
    
    if ( flag ) :
        appendingexcel.storeResult("Legitimate",url)
        return "yes"

    else:
        appendingexcel.storeResult("Phishing",url)
        return "no"
