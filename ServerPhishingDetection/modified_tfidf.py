from sklearn.feature_extraction.text import TfidfVectorizer
import xlrd

def isInTitle(TITLE,word):
    return TITLE.lower().find(word.lower())

def isInMeta(META,word):
    return META.lower().find(word.lower())

def getKeywords(URL,TITLE,META):
    loc = (r"H:\code\minorproject\ServerPhishingDetection\Book1.xlsx")
    book1 = xlrd.open_workbook(loc)
    read_sheet = book1.sheet_by_index(0)  
    corpus = []

    for i in range(len(read_sheet.col_values(0))):
        corpus.append(read_sheet.cell_value(i,3))
        corpus.append(read_sheet.cell_value(i,4))
        corpus.append(read_sheet.cell_value(i,5))
        if ( read_sheet.cell_value(i,0) == URL ):
            curr_val = i

    vectorizer = TfidfVectorizer()
    response = vectorizer.fit_transform(corpus)
    names = vectorizer.get_feature_names()
   
    
    for i in range (len(names)):
        if isInTitle(TITLE,names[i]) != -1:
           
            response[curr_val,i] *= 3
        if isInMeta(str(META),names[i]) != -1:
            response[curr_val,i] *= 2
         
    sorted_indices = (-response.toarray()).argsort()
    sorted_names = []

    keywords = ""
    for j in range (min(3,len(sorted_indices[curr_val]))):
        keywords += names[sorted_indices[curr_val,j]] + " "
    return keywords
