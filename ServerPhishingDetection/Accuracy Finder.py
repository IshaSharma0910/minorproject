import openpyxl

loc = (r"H:\code\minorproject\ServerPhishingDetection\Book1.xlsx")
book1 = openpyxl.load_workbook(loc)
sheet = book1["Sheet1"]

p = 0
q = 0
r = 0
s = 0
total = 0
for i in range (1,sheet.max_row +1):
    if sheet.cell(i,2).value == sheet.cell(i,3).value:
        if sheet.cell(i,2).value == "Phishing":
            p = p + 1
        else:
            s = s + 1
    else:
        if sheet.cell(i,2).value == "Legitimate" and sheet.cell(i,3).value == "Phishing":
            q = q + 1
        elif sheet.cell(i,2).value == "Phishing" and sheet.cell(i,3).value == "Legitimate":
            r = r + 1
    total = total + 1
    
tpr = p / (p + r)
tnr = s / (q + s)
accuracy = (p + s) / ( p + q + r + s) * 100
print("True Positive Ratio:",tpr)
print("True Negative Ratio:",tnr)
print("Accuracy:",accuracy)
